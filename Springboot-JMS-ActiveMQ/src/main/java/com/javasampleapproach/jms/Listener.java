package com.javasampleapproach.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Message;

/**
 * Created by N657051 on 5/28/2017.
 */
@Component
public class Listener {

    @JmsListener(destination = "DEMO-JMS-QUEUE", containerFactory = "myFactory")
    public void receivemessage(Message msg) {
        System.out.println("Received Message: "+msg);
    }

}
